package com.laptrinhjavaweb.paging;

import com.laptrinhjavaweb.sort.Sorter;

public interface Pageable {
	Integer getPage();
	Integer getMaxPageItem();
	Integer getOffset();
	Sorter getSort();
}
