package com.laptrinhjavaweb.utils;

import javax.servlet.http.HttpServletRequest;

public class MessageUtil {
	public static void showMessage(HttpServletRequest request) {
		String message = request.getParameter("message");
		String alert = "";
		String messageResp = "";
		if(message != null) {
			if(message.equals("insert_success")) {
				messageResp = "Insert success";
				alert = "success";
			}else if(message.equals("update_success")) {
				messageResp = "Update success";
				alert = "success";
			}else if(message.equals("delete_success")) {
				messageResp = "Delete success";
				alert = "success";
			}else if(message.equals("error_system")) {
				messageResp = "Error system";
				alert = "danger";
			}
			request.setAttribute("messageResponse", messageResp);
			request.setAttribute("alert", alert);
		}
	}
}
