package com.laptrinhjavaweb.controller.admin;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.laptrinhjavaweb.constant.SystemConstant;
import com.laptrinhjavaweb.model.NewModel;
import com.laptrinhjavaweb.paging.PageRequest;
import com.laptrinhjavaweb.paging.Pageable;
import com.laptrinhjavaweb.service.ICategoryService;
import com.laptrinhjavaweb.service.INewService;
import com.laptrinhjavaweb.sort.Sorter;
import com.laptrinhjavaweb.utils.FormUtil;
import com.laptrinhjavaweb.utils.MessageUtil;

@WebServlet(urlPatterns = {"/admin-new"})
public class NewController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Inject
    private INewService newService;

    @Inject
    private ICategoryService categoryService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        NewModel newModel = FormUtil.toModel(NewModel.class, req);
        String view = "";
        if (newModel.getType().equals(SystemConstant.LIST)) {
            Pageable pageable = new PageRequest(newModel.getPage(), newModel.getMaxPageItem(),
                    new Sorter(newModel.getSortName(), newModel.getSortBy()));
            List<NewModel> news = newService.findAll(pageable);
            newModel.setListResult(news);
            newModel.setTotalItem(newService.getTotalItem());
            int totalPage = (int) Math.ceil((double) newModel.getTotalItem() / newModel.getMaxPageItem());
            newModel.setTotalPage(totalPage == 0 ? 1 : totalPage);
            if (newModel.getTotalPage() < newModel.getPage()) {
                resp.sendRedirect(req.getContextPath() + "/admin-new?page=" + (newModel.getPage() - 1)
                        + "&maxPageItem=5&type=list&message=delete_success");
                return;
            } else {
                view = "/views/admin/new/list.jsp";
            }
        } else if (newModel.getType().equals(SystemConstant.EDIT)) {
            if (newModel.getId() != null) {
                newModel = newService.findOne(newModel.getId());
            }
            req.setAttribute("categories", categoryService.findAll());
            view = "/views/admin/new/edit.jsp";
        }
        MessageUtil.showMessage(req);
        req.setAttribute(SystemConstant.MODEL, newModel);
        RequestDispatcher rd = req.getRequestDispatcher(view);
        rd.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doGet(req, resp);
    }

}
