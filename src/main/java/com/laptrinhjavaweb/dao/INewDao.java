package com.laptrinhjavaweb.dao;

import java.util.List;

import com.laptrinhjavaweb.model.NewModel;
import com.laptrinhjavaweb.paging.Pageable;

public interface INewDao {
	NewModel findOne(Long id);
	List<NewModel> findByCategoryId(Long categoryId);
	Long save(NewModel newModel);
	void update(NewModel newModel);
	void delete(Long newId);
	//List<NewModel> findAll(int offset, int limit);
	List<NewModel> findAll(Pageable pageable);
	int getTotalItem();
}
