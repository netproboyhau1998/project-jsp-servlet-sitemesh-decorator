package com.laptrinhjavaweb.dao;

import java.util.List;

import com.laptrinhjavaweb.model.CategoryModel;

public interface ICategoryDao {
	List<CategoryModel> findAll();
	Long save(CategoryModel categoryModel);
	void update(CategoryModel categoryModel);
}
