package com.laptrinhjavaweb.dao.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import com.laptrinhjavaweb.dao.GenericDao;
import com.laptrinhjavaweb.mapper.RowMapper;

public class AbstractDao<T> implements GenericDao<T> {
	
	ResourceBundle bundle = ResourceBundle.getBundle("db");
	
	public Connection getConnection() {
		try {
//			Class.forName("com.mysql.jdbc.Driver");
//			String url = "jdbc:mysql://localhost:3306/jspservletjdbc";
//			String username = "root";
//			String password = "hau123";
			Class.forName(bundle.getString("driverName"));
			String url = bundle.getString("url");
			String username = bundle.getString("username");
			String password = bundle.getString("password");
			return DriverManager.getConnection(url, username, password);
		} catch (ClassNotFoundException | SQLException e) {
			return null;
		}
	}

	private void setParameter(PreparedStatement ps, Object... parameters) {
		for (int i = 0; i < parameters.length; i++) {
			Object parameter = parameters[i];
			int index = i + 1;
			try {
				if (parameter instanceof Long) {
					ps.setLong(index, (Long) parameter);
				} else if (parameter instanceof String) {
					ps.setString(index, (String) parameter);
				} else if (parameter instanceof Integer) {
					ps.setInt(index, (Integer) parameter);
				} else if (parameter instanceof Timestamp) {
					ps.setTimestamp(index, (Timestamp) parameter);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public List<T> query(String sql, RowMapper<T> rowMapper, Object... objects) {
		List<T> list = new ArrayList<T>();
		Connection c = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			c = this.getConnection();
			ps = c.prepareStatement(sql);
			this.setParameter(ps, objects);
			rs = ps.executeQuery();
			while (rs.next()) {
				list.add(rowMapper.mapper(rs));
			}
			return list;
		} catch (SQLException e) {
			return null;
		} finally {
			try {
				if (c != null) {
					c.close();
				}
				if (ps != null) {
					ps.close();
				}
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException e) {
				return null;
			}
		}
	}

	@Override
	public Long insert(String sql, Object... parameters) {
		Connection c = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			Long id = null;
			c = this.getConnection();
			c.setAutoCommit(false);
			ps = c.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			this.setParameter(ps, parameters);
			ps.executeUpdate();
			rs = ps.getGeneratedKeys();
			while (rs.next()) {
				id = rs.getLong(1);
			}
			c.commit();
			return id;
		} catch (SQLException e) {
			if (c != null) {
				try {
					c.rollback();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
				if (ps != null) {
					ps.close();
				}
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	@Override
	public void update(String sql, Object... parameters) {
		Connection c = null;
		PreparedStatement ps = null;
		try {
			c = this.getConnection();
			c.setAutoCommit(false);
			ps = c.prepareStatement(sql);
			this.setParameter(ps, parameters);
			ps.executeUpdate();
			c.commit();
		} catch (SQLException e) {
			if (c != null) {
				try {
					c.rollback();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
				if (ps != null) {
					ps.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public int count(String sql, Object... parameters) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			int count = 0;
			connection = getConnection();
			statement = connection.prepareStatement(sql);
			setParameter(statement, parameters);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				count = resultSet.getInt(1);
			}
			return count;
		} catch (SQLException e) {
			return 0;
		} finally {
			try {
				if (connection != null) {
					connection.close();
				}
				if (statement != null) {
					statement.close();
				}
				if (resultSet != null) {
					resultSet.close();
				}
			} catch (SQLException e) {
				return 0;
			}
		}
	}
}
