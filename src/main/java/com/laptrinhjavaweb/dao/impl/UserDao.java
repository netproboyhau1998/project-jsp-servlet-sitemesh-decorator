package com.laptrinhjavaweb.dao.impl;

import java.util.List;

import com.laptrinhjavaweb.dao.IUserDao;
import com.laptrinhjavaweb.mapper.UserMapper;
import com.laptrinhjavaweb.model.UserModel;

public class UserDao extends AbstractDao<UserModel> implements IUserDao {

	@Override
	public UserModel findByUserNameAndPasswordAndStatus(String username, String password, Integer status) {
		StringBuilder builder = new StringBuilder("SELECT * FROM user AS u");
		builder.append(" INNER JOIN role AS r ON r.id = u.roleid");
		builder.append(" WHERE username = ? AND password = ? AND status = ?");
		List<UserModel> list = this.query(builder.toString(), new UserMapper(), username, password, status);
		return list.isEmpty() ? null : list.get(0);
	}
}
