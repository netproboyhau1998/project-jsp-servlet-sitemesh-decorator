package com.laptrinhjavaweb.dao.impl;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.laptrinhjavaweb.dao.INewDao;
import com.laptrinhjavaweb.mapper.NewMapper;
import com.laptrinhjavaweb.model.NewModel;
import com.laptrinhjavaweb.paging.Pageable;

public class NewDao extends AbstractDao<NewModel> implements INewDao {

	@Override
	public List<NewModel> findByCategoryId(Long categoryId) {
		String sql = "SELECT * FROM news WHERE categoryid = ?";
		return this.query(sql, new NewMapper(), categoryId);
	}

	@Override
	public Long save(NewModel newModel) {
		StringBuilder builder = new StringBuilder("INSERT INTO news (");
		builder.append("title, thumbnail, shortDescription, content, categoryid, createddate, createdby)");
		builder.append(" VALUES (?,?,?,?,?,?,?)");
		return this.insert(builder.toString(), newModel.getTitle(), newModel.getThumbnail(),
				newModel.getShortDescription(), newModel.getContent(), newModel.getCategoryId(),
				newModel.getCreatedDate(), newModel.getCreatedBy());
	}

	@Override
	public void update(NewModel newModel) {
		StringBuilder builder = new StringBuilder("UPDATE news SET ");
		builder.append("title = ?, thumbnail = ?, shortDescription = ?, content = ?");
		builder.append(", categoryid = ?, createddate = ?, modifiedDate = ?, createdby = ?, modifiedBy = ?");
		builder.append(" WHERE id = ?");
		this.update(builder.toString(), newModel.getTitle(), newModel.getThumbnail(), newModel.getShortDescription(),
				newModel.getContent(), newModel.getCategoryId(), newModel.getCreatedDate(), newModel.getModifiedDate(),
				newModel.getCreatedBy(), newModel.getModifiedBy(), newModel.getId());
	}

	@Override
	public void delete(Long newId) {
		String sql = "DELETE FROM news WHERE id = ?";
		this.update(sql, newId);
	}

	@Override
	public NewModel findOne(Long id) {
		String sql = "SELECT * FROM news WHERE id = ?";
		List<NewModel> news = this.query(sql, new NewMapper(), id);
		return (news == null || news.isEmpty()) ? null : news.get(0);
	}

//	@Override
//	public List<NewModel> findAll(int offset, int limit) {
//		String sql = "SELECT * FROM news LIMIT ?,?";
//		return this.query(sql, new NewMapper(), offset, limit);
//	}

	@Override
	public int getTotalItem() {
		String sql = "SELECT count(*) FROM news";
		return this.count(sql);
	}

	@Override
	public List<NewModel> findAll(Pageable pageable) {
		StringBuilder builder = new StringBuilder("SELECT * FROM news");
		if (pageable.getSort() != null && StringUtils.isNotBlank(pageable.getSort().getSortBy())
				&& StringUtils.isNotBlank(pageable.getSort().getSortName())) {
			builder.append(" ORDER BY " + pageable.getSort().getSortName() + " " + pageable.getSort().getSortBy());
		}
		if (pageable.getPage() != null && pageable.getMaxPageItem() != null) {
			builder.append(" LIMIT " + pageable.getOffset() + ", " + pageable.getMaxPageItem());
		}
		return this.query(builder.toString(), new NewMapper());
	}
}
