package com.laptrinhjavaweb.dao.impl;

import java.util.List;

import com.laptrinhjavaweb.dao.ICategoryDao;
import com.laptrinhjavaweb.mapper.CategoryMapper;
import com.laptrinhjavaweb.model.CategoryModel;

public class CategoryDao extends AbstractDao<CategoryModel> implements ICategoryDao {

	@Override
	public List<CategoryModel> findAll() {
		String sql = "SELECT * FROM category";
		return this.query(sql, new CategoryMapper());
	}

	@Override
	public Long save(CategoryModel categoryModel) {
		String sql = "INSERT INTO category (name, code) VALUES (?,?)";
		return this.insert(sql, categoryModel.getName(), categoryModel.getCode());
	}

	@Override
	public void update(CategoryModel categoryModel) {
		String sql = "UPDATE category SET name = ?, code = ? where id = ?";
		this.update(sql, categoryModel.getName(), categoryModel.getCode(), categoryModel.getId());
	}
}
