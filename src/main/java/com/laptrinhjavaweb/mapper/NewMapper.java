package com.laptrinhjavaweb.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.laptrinhjavaweb.model.NewModel;

public class NewMapper implements RowMapper<NewModel> {

	@Override
	public NewModel mapper(ResultSet rs) {
		try {
			NewModel newModel = new NewModel();
			newModel.setId(rs.getLong("id"));
			newModel.setTitle(rs.getString("title"));
			newModel.setContent(rs.getString("content"));
			newModel.setCategoryId(rs.getLong("categoryid"));
			newModel.setThumbnail(rs.getString("thumbnail"));
			newModel.setShortDescription(rs.getString("shortdescription"));
			newModel.setCreatedDate(rs.getTimestamp("createddate"));
			newModel.setCreatedBy(rs.getString("createdby"));
			if (rs.getString("modifiedby") != null) {
				newModel.setModifiedBy(rs.getString("modifiedby"));
			}
			if (rs.getString("modifieddate") != null) {
				newModel.setModifiedDate(rs.getTimestamp("modifieddate"));
			}
			return newModel;
		} catch (SQLException e) {
			return null;
		}
	}

}
