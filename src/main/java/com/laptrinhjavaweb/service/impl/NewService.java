package com.laptrinhjavaweb.service.impl;

import java.sql.Timestamp;
import java.util.List;

import javax.inject.Inject;

import com.laptrinhjavaweb.dao.INewDao;
import com.laptrinhjavaweb.model.NewModel;
import com.laptrinhjavaweb.paging.Pageable;
import com.laptrinhjavaweb.service.INewService;

public class NewService implements INewService {
	@Inject
	private INewDao newDao;

	@Override
	public List<NewModel> findByCategoryId(Long categoryId) {
		return newDao.findByCategoryId(categoryId);
	}

	@Override
	public NewModel save(NewModel newModel) {
		newModel.setCreatedDate(new Timestamp(System.currentTimeMillis()));
		//newModel.setCreatedBy("Hau");
		Long id = newDao.save(newModel);
		return newDao.findOne(id);
	}

	@Override
	public NewModel update(NewModel updateNew) {
		NewModel oldNew = newDao.findOne(updateNew.getId());
		updateNew.setCreatedBy(oldNew.getCreatedBy());
		updateNew.setCreatedDate(oldNew.getCreatedDate());
		//updateNew.setModifiedBy("Hau");
		updateNew.setModifiedDate(new Timestamp(System.currentTimeMillis()));
		newDao.update(updateNew);
		return newDao.findOne(updateNew.getId());
	}

	@Override
	public void delete(Long newId) {
		newDao.delete(newId);
	}

//	@Override
//	public List<NewModel> findAll(int offset, int limit) {
//		return newDao.findAll(offset, limit);
//	}

	@Override
	public int getTotalItem() {
		return newDao.getTotalItem();
	}

	@Override
	public List<NewModel> findAll(Pageable pageable) {
		return newDao.findAll(pageable);
	}

	@Override
	public NewModel findOne(Long id) {
		return newDao.findOne(id);
	}

	@Override
	public void delete(Long[] ids) {
		for(Long id : ids) {
			newDao.delete(id);
		}
	}

}
