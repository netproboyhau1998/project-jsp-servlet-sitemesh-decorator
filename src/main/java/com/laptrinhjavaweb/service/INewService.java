package com.laptrinhjavaweb.service;

import java.util.List;

import com.laptrinhjavaweb.model.NewModel;
import com.laptrinhjavaweb.paging.Pageable;

public interface INewService {
	List<NewModel> findByCategoryId(Long categoryId);
	NewModel save(NewModel newModel);
	NewModel update(NewModel newModel);
	void delete(Long newId);
	//List<NewModel> findAll(int offset, int limit);
	int getTotalItem();
	List<NewModel> findAll(Pageable pageable);
	NewModel findOne(Long id);
	void delete(Long[] ids);
}
