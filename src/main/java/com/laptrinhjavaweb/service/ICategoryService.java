package com.laptrinhjavaweb.service;

import java.util.List;

import com.laptrinhjavaweb.model.CategoryModel;

public interface ICategoryService {
	List<CategoryModel> findAll();
	Long save(CategoryModel categoryModel);
	void update(CategoryModel categoryModel);
}
