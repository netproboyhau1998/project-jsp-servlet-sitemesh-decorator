<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/common/taglib.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Đăng nhập</title>
</head>
<body>
	<div class="container">
		<h1 class="form-heading">Login Form</h1>
		<div class="login-form">
			<div class="main-div">
				<div class="alert alert-${alert}">
					${message}
				</div>
				<div class="panel">
					<h2>Admin Login</h2>
					<p>Please enter your email and password</p>
				</div>
				<form action="<c:url value='/dang-nhap'/>" id="formLogin"
					method="post">
					<div class="form-group">
						<input type="text" class="form-control" id="userName"
							name="userName" placeholder="Username">
					</div>
					<div class="form-group">
						<input type="password" class="form-control" id="password"
							name="password" placeholder="Password">
					</div>
					<div class="forgot">
						<a href="reset.html">Forgot password?</a>
					</div>
					<input type="hidden" value="login" name="action" />
					<button type="submit" class="btn btn-primary">Login</button>
				</form>
			</div>
			<p class="botto-text">Designed by Hau</p>
		</div>
	</div>
</body>
</html>